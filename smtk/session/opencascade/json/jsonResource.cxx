#include "smtk/session/opencascade/json/jsonResource.h"

#include "smtk/resource/json/jsonResource.h"

namespace smtk
{
namespace session
{
namespace opencascade
{
namespace detail
{

template<typename Visitor>
void VisitChildren(TopoDS_Shape& shape, Visitor visitor)
{
  // NOTE: The usage of this API requires the use of the TopoDS_Iterator in order
  // to guarentee an ordering that is consistent with the OCCT backend
  TopoDS_Iterator it;
  for (it.Initialize(shape); it.More(); it.Next())
  {
    auto subshape = it.Value();
    if (visitor(subshape))
    {
      break;
    }
  }
};
}

void to_json(nlohmann::json& j, const smtk::session::opencascade::Resource::Ptr& resource)
{
  smtk::resource::to_json(j, std::static_pointer_cast<smtk::resource::Resource>(resource));

  // Get the OCCT session
  auto session = resource->session();
  TopoDS_Shape& compound = resource->compound();

  // Create list of Topology enitity UUIDS
  std::vector<smtk::common::UUID> uids;
  std::set<smtk::common::UUID> inserted_uids;

  std::function<bool(TopoDS_Shape&)> visitor = [&](TopoDS_Shape& shape) -> bool {
    auto uid = session->findID(shape);
    if (!uid.isNull() && inserted_uids.find(uid) == inserted_uids.end())
    {
      uids.push_back(uid);
      inserted_uids.insert(uid);
      detail::VisitChildren(shape, visitor);
    }
    // Don't stop
    return false;
  };
  visitor(compound);

  j["preservedUUIDs"] = uids;
}

void from_json(const nlohmann::json& j, smtk::session::opencascade::Resource::Ptr& resource)
{
  auto tmp = std::static_pointer_cast<smtk::resource::Resource>(resource);
  smtk::resource::from_json(j, tmp);

  auto session = resource->session();
  auto compound = resource->compound();

  std::vector<smtk::common::UUID> uids = j["preservedUUIDs"];
  std::vector<smtk::common::UUID> uidStack(uids.rbegin(), uids.rend());
  std::function<bool(TopoDS_Shape&)> visitor = [&](TopoDS_Shape& shape) {
    if (session->findID(shape).isNull())
    {
      if(uidStack.empty()) {
        // Stop if there are no more IDs to assign
        throw std::runtime_error("It appears the OpenCascade model has changed since the SMTK file was written – there are more components than SMTK recorded.");
      }
      auto node = resource->createShape(shape);
      node->setId(uidStack.back());
      uidStack.pop_back();
      session->addShape(node->id(), shape);
      detail::VisitChildren(shape, visitor);
    }
    // Don't stop
    return false;
  };
  visitor(compound);
  if (!uidStack.empty())
  {
    throw std::runtime_error("It appears the OpenCascade model has changed since the SMTK file was written – there are fewer components than SMTK recorded.");
  }
}

} // namespace opencascade
} // namespace session
} // namespace smtk
