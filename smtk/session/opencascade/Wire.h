//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_Wire_h
#define smtk_session_opencascade_Wire_h

#include "smtk/session/opencascade/Shape.h"
#include "smtk/session/opencascade/Traits.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

class Face;
class Edge;

class SMTKOPENCASCADESESSION_EXPORT Wire : public Shape
{
public:
  smtkTypeMacro(Wire);
  smtkSuperclassMacro(smtk::session::opencascade::Shape);

  Wire(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc);

  /**\brief Return the container of faces referencing this wire.
    */
  //@{
  ArcEndpointInterface<FacesToWires, ConstArc, IncomingArc> faces() const;
  ArcEndpointInterface<FacesToWires, NonConstArc, IncomingArc> faces();
  //@}

  /**\brief Return the container of edges composing this wire.
    */
  //@{
  ArcEndpointInterface<WiresToEdges, ConstArc, OutgoingArc> edges() const;
  ArcEndpointInterface<WiresToEdges, NonConstArc, OutgoingArc> edges();
  //@}
};
}
}
}

#endif
