//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_Face_h
#define smtk_session_opencascade_Face_h

#include "smtk/session/opencascade/Shape.h"
#include "smtk/session/opencascade/Traits.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

class Shell;
class Wire;

class SMTKOPENCASCADESESSION_EXPORT Face : public Shape
{
public:
  smtkTypeMacro(smtk::session::opencascade::Face);
  smtkSuperclassMacro(smtk::session::opencascade::Shape);

  Face(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc);

  /**\brief Return the container of shells referencing this face.
    */
  //@{
  ArcEndpointInterface<ShellsToFaces, ConstArc, IncomingArc> shells() const;
  ArcEndpointInterface<ShellsToFaces, NonConstArc, IncomingArc> shells();
  //@}

  /**\brief Return the container of wires composing this face.
    */
  //@{
  ArcEndpointInterface<FacesToWires, ConstArc, OutgoingArc> wires() const;
  ArcEndpointInterface<FacesToWires, NonConstArc, OutgoingArc> wires();
  //@}
};
}
}
}

#endif
