//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_ParentChild_h
#define smtk_session_opencascade_ParentChild_h

#include "smtk/graph/Resource.h"
#include "smtk/session/opencascade/Shape.h"

#include "smtk/common/CompilerInformation.h"

#include "TopoDS_Iterator.hxx"
#include "TopoDS_Shape.hxx"
#include "TopExp.hxx"

namespace smtk
{
namespace session
{
namespace opencascade
{

template<typename XParentType, typename XChildType>
class ParentChild
{
public:
  using FromType = XParentType;
  using ToType = XChildType;
  using Directed = std::true_type;
  // using Mutable = std::false_type;

  template<typename Functor>
  smtk::common::Visited outVisitor(const FromType* component, Functor&& ff) const
  {
    auto result = smtk::common::Visited::Empty;
    if (!component)
    {
      return result;
    }
    auto resource = component->occResource();
    auto session = resource->session();
    auto shape = session->findShape(component->id());
    if (!shape)
    {
      return result;
    }
    smtk::common::VisitorFunctor<Functor> visitor(ff);
    TopoDS_Iterator it;
    for (it.Initialize(*shape); it.More(); it.Next())
    {
      auto uid = session->findID(it.Value());
      if (!uid)
      {
        continue;
      }
      auto node = dynamic_cast<const ToType*>(resource->find(uid).get());
      if (!node)
      {
        continue;
      }
      result = smtk::common::Visited::Some;
      if (visitor(node) == smtk::common::Visit::Halt)
      {
        return result;
      }
    }
    return result == smtk::common::Visited::Some ?
      smtk::common::Visited::All : smtk::common::Visited::Empty;
  }

  template<typename Functor>
  smtk::common::Visited inVisitor(const ToType* component, Functor&& ff) const
  {
    auto result = smtk::common::Visited::Empty;
    if (!component)
    {
      return result;
    }
    auto resource = component->occResource();
    auto session = resource->session();

    if (auto shape = component->data())
    {
      // If our shape is a compound, it has no parent
      auto shapeType = shape->ShapeType();
      if (shapeType == TopAbs_COMPOUND)
      {
        return result;
      }

      TopTools_IndexedDataMapOfShapeListOfShape map;
      TopExp::MapShapesAndAncestors(resource->compound(), shapeType,
        static_cast<TopAbs_ShapeEnum>(static_cast<int>(shapeType) - 1), map);

      smtk::common::VisitorFunctor<Functor> visitor(ff);
      const TopTools_ListOfShape& parents = map.FindFromKey(*shape);
      for (const TopoDS_Shape& parent : parents)
      {
        auto uid = session->findID(parent);
        if (!uid)
        {
          continue;
        }
        auto node = dynamic_cast<FromType*>(resource->component(uid));
        if (!node)
        {
          continue;
        }
        result = smtk::common::Visited::Some;
        if (visitor(node) == smtk::common::Visit::Halt)
        {
          return result;
        }
      }
    }
    return result == smtk::common::Visited::Some ?
      smtk::common::Visited::All : smtk::common::Visited::Empty;
  }
};

}
}
}

#endif
