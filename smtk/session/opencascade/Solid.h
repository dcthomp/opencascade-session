//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_Solid_h
#define smtk_session_opencascade_Solid_h

#include "smtk/session/opencascade/Shape.h"
#include "smtk/session/opencascade/Traits.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

class CompSolid;
class Shell;

class SMTKOPENCASCADESESSION_EXPORT Solid : public Shape
{
public:
  smtkTypeMacro(smtk::session::opencascade::Solid);
  smtkSuperclassMacro(smtk::session::opencascade::Shape);

  Solid(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc);

  /**\brief Return the container of compsolids referencing this solid.
    */
  //@{
  ArcEndpointInterface<CompSolidsToSolids, ConstArc, IncomingArc> compsolids() const;
  ArcEndpointInterface<CompSolidsToSolids, NonConstArc, IncomingArc> compsolids();
  //@}

  /**\brief Return the container of shells composing this solid.
    */
  //@{
  ArcEndpointInterface<SolidsToShells, ConstArc, OutgoingArc> shells() const;
  ArcEndpointInterface<SolidsToShells, NonConstArc, OutgoingArc> shells();
  //@}
};
}
}
}

#endif
