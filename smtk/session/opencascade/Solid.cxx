//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/opencascade/Solid.h"

#include "smtk/session/opencascade/CompSolid.h"
#include "smtk/session/opencascade/Shell.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

Solid::Solid(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc)
  : Shape(rsrc)
{
}

ArcEndpointInterface<CompSolidsToSolids, ConstArc, IncomingArc> Solid::compsolids() const
{
  return this->incoming<CompSolidsToSolids>();
}

ArcEndpointInterface<CompSolidsToSolids, NonConstArc, IncomingArc> Solid::compsolids()
{
  return this->incoming<CompSolidsToSolids>();
}

ArcEndpointInterface<SolidsToShells, ConstArc, OutgoingArc> Solid::shells() const
{
  return this->outgoing<SolidsToShells>();
}

ArcEndpointInterface<SolidsToShells, NonConstArc, OutgoingArc> Solid::shells()
{
  return this->outgoing<SolidsToShells>();
}

} // namespace opencascade
} // namespace session
} // namespace smtk
