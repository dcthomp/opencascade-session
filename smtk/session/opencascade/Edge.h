//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_Edge_h
#define smtk_session_opencascade_Edge_h

#include "smtk/session/opencascade/Shape.h"
#include "smtk/session/opencascade/Traits.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

class Wire;
class Vertex;

class SMTKOPENCASCADESESSION_EXPORT Edge : public Shape
{
public:
  smtkTypeMacro(smtk::session::opencascade::Edge);
  smtkSuperclassMacro(smtk::session::opencascade::Shape);

  Edge(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc);

  /**\brief Return the container of wires referencing this edge.
    */
  //@{
  ArcEndpointInterface<WiresToEdges, ConstArc, IncomingArc> wires() const;
  ArcEndpointInterface<WiresToEdges, NonConstArc, IncomingArc> wires();
  //@}

  /**\brief Return the container of vertices composing this edge.
    */
  //@{
  ArcEndpointInterface<EdgesToVertices, ConstArc, OutgoingArc> vertices() const;
  ArcEndpointInterface<EdgesToVertices, NonConstArc, OutgoingArc> vertices();
  //@}
};
}
}
}

#endif
