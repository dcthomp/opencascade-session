//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_Read_h
#define smtk_session_opencascade_Read_h

#include "smtk/session/opencascade/Operation.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

class SMTKOPENCASCADESESSION_EXPORT Read : public Operation
{
public:
  smtkTypeMacro(smtk::session::opencascade::Read);
  smtkCreateMacro(Read);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(Operation);

protected:
  /// This method does the bulk of the work importing model data.
  Result operateInternal() override;
  /// Return XML describing the operation inputs and outputs as attributes.
  virtual const char* xmlDescription() const override;
};

SMTKOPENCASCADESESSION_EXPORT smtk::resource::ResourcePtr read(
  const std::string& filename,
  const std::shared_ptr<smtk::common::Managers>& managers);

} // namespace opencascade
} // namespace session
} // namespace smtk

#endif // smtk_session_opencascade_Read_h
