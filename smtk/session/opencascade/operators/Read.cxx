//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/opencascade/operators/Read.h"

#include "smtk/session/opencascade/operators/Read_xml.h"

#include "smtk/session/opencascade/Resource.h"
#include "smtk/session/opencascade/Session.h"
#include "smtk/session/opencascade/json/jsonResource.h"

#include "smtk/operation/MarkGeometry.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ReferenceItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"

#include "smtk/resource/Manager.h"

#include "smtk/io/Logger.h"

#include "smtk/common/Paths.h"

#include "BRepTools.hxx"
#include "BRep_Builder.hxx"
#include "IGESCAFControl_Reader.hxx"
#include "STEPCAFControl_Reader.hxx"
#include "TopoDS_Compound.hxx"
#include "TopoDS_Iterator.hxx"
#include "TopoDS_Shape.hxx"
#include "Transfer_TransientProcess.hxx"
#include "XCAFDoc_DocumentTool.hxx"
#include "XCAFDoc_ShapeTool.hxx"
#include "XSControl_TransferReader.hxx"
#include "XSControl_WorkSession.hxx"

#include <algorithm>
#include <cctype>
#include <mutex>
#include <string>

namespace smtk
{
namespace session
{
namespace opencascade
{

Read::Result Read::operateInternal()
{
  smtk::session::opencascade::Resource::Ptr resource;
  smtk::session::opencascade::Session::Ptr session;

  auto result = this->createResult(Read::Outcome::FAILED);

  this->prepareResourceAndSession(result, resource, session);

  std::string filename = this->parameters()->findFile("filename")->value();

  std::ifstream file(filename);
  if (!file.good())
  {
    smtkErrorMacro(log(), "Cannot read file \"" << filename << "\".");
    file.close();
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  nlohmann::json j;
  try
  {
    j = nlohmann::json::parse(file);
  }
  catch (...)
  {
    smtkErrorMacro(log(), "Cannot parse file \"" << filename << "\".");
    file.close();
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }
  file.close();

  // Read the Brep file
  std::string brep_filename = j.at("filename");
  BRep_Builder builder;
  TopoDS_Shape& shape = resource->compound();
  {
    auto ok = BRepTools::Read(shape, brep_filename.c_str(), builder);
    if (!ok)
    {
      return this->createResult(smtk::operation::Operation::Outcome::FAILED); // Failure
    }
  }

  // Deserialize resource
  try
  {
    from_json(j, resource);
  }
  catch(std::exception& e)
  {
    smtkErrorMacro(log(), e.what());
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  // Mark Compound/Face/Edge/Vertex entities as modifed
  {
    auto uid = session->findID(shape);
    if (uid.isNull())
    {
      smtkErrorMacro(log(), "Could not deserialize BRep");
      return this->createResult(smtk::operation::Operation::Outcome::FAILED);
    }
    auto topNode = dynamic_cast<Shape*>(resource->find(uid).get());
    if (!topNode)
    {
      smtkErrorMacro(log(), "Could not find OCCT Compound shape");
      return this->createResult(smtk::operation::Operation::Outcome::FAILED); // Failure
    }
    std::set<smtk::common::UUID> visitedUIds;
    Shape::Visitor visitor;
    visitor = [&](Shape* node) -> bool {
      // Only visit entities once
      if(visitedUIds.find(node->id()) == visitedUIds.end())
      {
        visitedUIds.insert(node->id());
        auto shapeType = node->data()->ShapeType();
        if ((shapeType == TopAbs_COMPOUND || shapeType == TopAbs_FACE || shapeType == TopAbs_EDGE ||
              shapeType == TopAbs_VERTEX))
        {
          operation::MarkGeometry(resource->shared_from_this())
            .markModified(node->shared_from_this());
        }
        node->visitSubshapes(visitor);
      }
      // Don't stop
      return false;
    };
    visitor(topNode);

    result->findComponent("created")->appendValue(topNode->shared_from_this());
  }

  result->findInt("outcome")->setValue(static_cast<int>(Read::Outcome::SUCCEEDED));

  return result;
}

const char* Read::xmlDescription() const
{
  return Read_xml;
}

smtk::resource::ResourcePtr read(
  const std::string& filename,
  const std::shared_ptr<smtk::common::Managers>& managers)
{
  Read::Ptr read = Read::create();
  read->setManagers(managers);
  read->parameters()->findFile("filename")->setValue(filename);
  Read::Result result = read->operate();
  if (result->findInt("outcome")->value() != static_cast<int>(Read::Outcome::SUCCEEDED))
  {
    return smtk::resource::ResourcePtr();
  }
  return result->findResource("resource")->value();
}

} // namespace openscacade
} // namespace session
} // namespace smtk
