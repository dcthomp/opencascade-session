//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/opencascade/CompSolid.h"

#include "smtk/session/opencascade/Compound.h"
#include "smtk/session/opencascade/Solid.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

CompSolid::CompSolid(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc)
  : Shape(rsrc)
{
}

ArcEndpointInterface<CompoundsToCompSolids, ConstArc, IncomingArc> CompSolid::compounds() const
{
  return this->incoming<CompoundsToCompSolids>();
}

ArcEndpointInterface<CompoundsToCompSolids, NonConstArc, IncomingArc> CompSolid::compounds()
{
  return this->incoming<CompoundsToCompSolids>();
}

ArcEndpointInterface<CompSolidsToSolids, ConstArc, OutgoingArc> CompSolid::solids() const
{
  return this->outgoing<CompSolidsToSolids>();
}

ArcEndpointInterface<CompSolidsToSolids, NonConstArc, OutgoingArc> CompSolid::solids()
{
  return this->outgoing<CompSolidsToSolids>();
}

} // namespace opencascade
} // namespace session
} // namespace smtk
