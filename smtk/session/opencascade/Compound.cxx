//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/opencascade/Compound.h"

#include "smtk/session/opencascade/CompSolid.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

Compound::Compound(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc)
  : Shape(rsrc)
{
}

ArcEndpointInterface<CompoundsToCompSolids, ConstArc, OutgoingArc> Compound::compsolids() const
{
  return this->outgoing<CompoundsToCompSolids>();
}

ArcEndpointInterface<CompoundsToCompSolids, NonConstArc, OutgoingArc> Compound::compsolids()
{
  return this->outgoing<CompoundsToCompSolids>();
}

} // namespace opencascade
} // namespace session
} // namespace smtk
