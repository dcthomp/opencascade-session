//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/opencascade/Edge.h"

#include "smtk/session/opencascade/Vertex.h"
#include "smtk/session/opencascade/Wire.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

Edge::Edge(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc)
  : Shape(rsrc)
{
}

ArcEndpointInterface<WiresToEdges, ConstArc, IncomingArc> Edge::wires() const
{
  return this->incoming<WiresToEdges>();
}

ArcEndpointInterface<WiresToEdges, NonConstArc, IncomingArc> Edge::wires()
{
  return this->incoming<WiresToEdges>();
}

ArcEndpointInterface<EdgesToVertices, ConstArc, OutgoingArc> Edge::vertices() const
{
  return this->outgoing<EdgesToVertices>();
}

ArcEndpointInterface<EdgesToVertices, NonConstArc, OutgoingArc> Edge::vertices()
{
  return this->outgoing<EdgesToVertices>();
}

} // namespace opencascade
} // namespace session
} // namespace smtk
