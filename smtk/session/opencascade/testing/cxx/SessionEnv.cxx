#include "SessionEnv.h"

#include "smtk/session/opencascade/Registrar.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

struct SessionEnv::Internals
{
  Internals()
  {
    // Create a resource manager
    m_resourceManager = smtk::resource::Manager::create();

    // Register opencascade resources to the resource manager
    {
      smtk::session::opencascade::Registrar::registerTo(m_resourceManager);
    }

    // Create an operation manager
    m_operationManager = smtk::operation::Manager::create();

    // Register opencascade operators to the operation manager
    {
      smtk::session::opencascade::Registrar::registerTo(m_operationManager);
    }

    // Register the resource manager to the operation manager (newly created
    // resources will be automatically registered to the resource manager).
    m_operationManager->registerResourceManager(m_resourceManager);
  }

  smtk::operation::Manager::Ptr m_operationManager;
  smtk::resource::Manager::Ptr m_resourceManager;
};

SessionEnv::SessionEnv() :
  m_internals(new SessionEnv::Internals)
{}

smtk::operation::ManagerPtr SessionEnv::operationManager()
{
  return m_internals->m_operationManager;
}

smtk::resource::ManagerPtr SessionEnv::resourceManager()
{
  return m_internals->m_resourceManager;
}

} // namespace opencascade
} // namespace session
} // namespace smtk
