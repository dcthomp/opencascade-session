#pragma once

#include "smtk/resource/Manager.h"
#include "smtk/operation/Manager.h"

#include <memory>

namespace smtk
{
namespace session
{
namespace opencascade
{

class SessionEnv
{
public:
  SessionEnv();

  smtk::operation::ManagerPtr operationManager();
  smtk::resource::ManagerPtr resourceManager();
private:
  struct Internals;
  std::shared_ptr<Internals> m_internals;
};

} // namespace opencascade
} // namespace session
} // namespace smtk
